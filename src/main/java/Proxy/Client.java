//============================================================================
// Name        : Client.java
// Created on  : August 30, 2020
// Author      : Tokmakov Andrey
// Version     : 0.1
// Copyright   : Your copyright notice
// Description : Proxy design pattern demo
//============================================================================

package Proxy;

import java.util.ArrayList;
import java.util.List;

// A very simple real life scenario is our college internet, which restricts
// few site access. The proxy first checks the host you are connecting to, if it
// is not part of restricted site list, then it connects to the real internet. 
// This example is based on Protection proxies.

/** Internet interface. **/
interface Internet  { 
	public void connectTo(String serverhost) throws Exception; 
}

/** RealInternet class. **/
class RealInternet implements Internet { 
	@Override
	public void connectTo(String serverhost) { 
		System.out.println("Connecting to "+ serverhost); 
	} 
} 

/** ProxyInternet class. **/
class ProxyInternet implements Internet { 
	private Internet internet = new RealInternet(); 
	private static List<String> bannedSites; 
	
	static { 
		bannedSites = new ArrayList<String>(); 
		bannedSites.add("abc.com"); 
		bannedSites.add("def.com"); 
		bannedSites.add("ijk.com"); 
		bannedSites.add("lnm.com"); 
	} 
	
	@Override
	public void connectTo(String serverhost) throws Exception { 
		if (bannedSites.contains(serverhost.toLowerCase())) { 
			throw new Exception(String.format("Access Denied to resource '%s'", serverhost)); 
		} 	
		internet.connectTo(serverhost); 
	} 
} 


public class Client {
	public static void main (String[] args) 
	{ 
		Internet internet = new ProxyInternet(); 
		try { 
			internet.connectTo("geeksforgeeks.org"); 
			internet.connectTo("abc.com"); 
		} 
		catch (Exception e)  { 
			System.out.println(e.getMessage()); 
		} 
	} 
}
