package Flyweight;

import java.util.ArrayList;

class CoffeeShop {
	
    private final ArrayList<Order> orders = new ArrayList<>();

    public void takeOrder(final String flavour, 
    		              int tableNumber) {
        orders.add(Order.valueOf(flavour, tableNumber));
    }

    public void service() {
        orders.forEach(Order::serve);
    }
}