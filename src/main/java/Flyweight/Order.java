package Flyweight;

@FunctionalInterface
interface Order {
    void serve();

    public static Order valueOf(final String flavourName, 
    		             	    int tableNumber) {
        CoffeeFlavour flavour = CoffeeFlavour.intern(flavourName);
        return () -> System.out.println("Serving " + flavour + " to table " + tableNumber);
    }
}